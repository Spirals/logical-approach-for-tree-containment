
#def merge_all(conf, tree, param):
#    # Merge the pebbles on the same node
#    list_conf = list(conf.keys()).copy()
#    for i, p1 in enumerate(list_conf):
#        for p2 in list_conf[i+1:]:
#            if conf[p1]!= None and conf[p1]==conf[p2]:
#                # the pebbles are on the same node
#                if p1[0] != p2[0]:
#                    # they should'nt merge according to the tree pattern
#                    if param["verbose"]:
#                        print("Wrong merge with {} and {} on {}"\
#                              .format(p1, p2, conf[p1]))
#                    return False
#                # or they merge and give an other pebble
#                if param["verbose"]:
#                    print("MERGE ", p1,p2, " on ", conf[p1])
#                param["map"][conf[p1]] = p1[0]
#                conf[(tree[p1[0]]["parents"][0], p1[0])] = conf[p1]
#                conf[p1] = None
#                conf[p2] = None
#    return True

#like merge_all but return the list of newly created pebbles
def merge_all2(conf, tree, param):
    # Merge the pebbles on the same node
    list_conf = list(conf.keys()).copy()
    just_merged = [] #list of newly created pebbles
    for i, p1 in enumerate(list_conf):
        for p2 in list_conf[i+1:]:
            if conf[p1]!= None and conf[p1]==conf[p2]:
                # the pebbles are on the same node
                if p1[0] != p2[0]:
                    # they should'nt merge according to the tree pattern
                    if param["verbose"]:
                        print("Wrong merge with {} and {} on {}"\
                              .format(p1, p2, conf[p1]))
                    return None
                # or they merge and give an other pebble
                if param["verbose"]:
                    print("MERGE ", p1,p2, " on ", conf[p1])
                new_pebble = (tree[p1[0]]["parents"][0], p1[0])
                just_merged.append(new_pebble)
                param["map"][conf[p1]] = p1[0]
                conf[new_pebble] = conf[p1]
                conf[p1] = None
                conf[p2] = None
    return just_merged

# Merge the pebbles on the same node it there are else find a pebble on a lowest level
def OM_AC_BU(conf, tree, network, param):
    lowest_pbl = None
    list_conf = list(conf.keys()).copy()
    for i, p1 in enumerate(list_conf):
        for p2 in list_conf[i+1:]:
            if conf[p1]!= None and conf[p1]==conf[p2]:
                # the pebbles are on the same node
                if p1[0] != p2[0]:
                    # they should'nt merge according to the tree pattern
                    if param["verbose"]:
                        print("Wrong merge with {} and {} on {}"\
                              .format(p1, p2, conf[p1]))
                    return False, None
                # or they merge and give an other pebble
                if param["verbose"]:
                    print("MERGE ", p1,p2, " on ", conf[p1])
                param["map"][conf[p1]] = p1[0]
                conf[(tree[p1[0]]["parents"][0], p1[0])] = conf[p1]
                conf[p1] = None
                conf[p2] = None
                return True, [(conf, param)]
            else:
                lowest_pbl = min(p1, p2, lowest_pbl, key=lambda x:network[conf[x]]["level"] if x is not None and conf[x] is not None else len(network))
    return False, lowest_pbl


def BM_TC_BU(conf, tree, param):
    # Merge the pebbles on the same node
    list_conf = list(conf.keys()).copy()
    for i, p1 in enumerate(list_conf):
        for p2 in list_conf[i+1:]:
            if conf[p1][1] and conf[p1]==conf[p2]:
                # the pebbles are active and on the same node
                if p1[0] == p2[1]:
                    # p1 is on its ending point and p2 at its starting point
                    if param["verbose"]:
                        print(p1, " is on its ending point at ", conf[p1][0], "(met ", p2, ")")
                    conf[p1] = conf[p1][0], False
                    param["map"][conf[p1][0]] = p1[0]
                elif p2[0] == p1[1]:
                    # p2 is on its ending point and p1 at its starting point
                    if param["verbose"]:
                        print(p2, " is on its ending point at ", conf[p2][0], "(met ", p1, ")")
                    conf[p2] = conf[p2][0], False
                    param["map"][conf[p2][0]] = p2[0]
                elif p1[0] != p2[0]:
                    # they should'nt merge according to the tree pattern
                    if param["verbose"]:
                        print("Wrong merge with {} and {} on {}"\
                              .format(p1, p2, conf[p1][0]))
                    return None
    return list_conf
