# BU-TC-CBM
# Comme block_tc.py mais avec config totale : place aléatoirement toutes les pierres au début
# Comme naive algo, but with not Conflicting block movements

import sys
import utils
from construct_graph import add_to_graph, clean_name
import itertools
import time
from copy import deepcopy
from init_tc import BM_TC_BU as init_game
from merge import BM_TC_BU as merge_all

# Return True if the given tree is contained in the given network, False otherwise
def tc(f_tree, f_network, verbose=False):
    start_load = time.time()
    tree, pebbles = get_graph(f_tree)
    network,_ = get_graph(f_network)
    start = time.time()
    done, res = init_game(tree, network, pebbles)
    after_init = time.time()
    if not done:
        return False, {"max_depth":0, "facts":0, "pb":res, "time":time.time()-start, "loading":start-start_load, "init":after_init-start }
    tree, network, possible_confs = res
    if verbose:
        utils.print_graphs(tree, network)
    
    contained = False
    facts = 0

    for conf in possible_confs:
        # run the pebble game with all possible configurations
        if verbose:
            print("---------------------")
            print(conf)
            print("---------------------")
        contained, param = move_pebbles(tree, network, conf, verbose)
        facts += param["facts"]
        if contained:
            # until if finds a solution
            break;

    end = time.time()
    param["time"] = end-after_init
    param["init"] = after_init-start
    param["loading"] = start-start_load
    param["total"] = end-start
    return contained, param


# given a configuration of the pebble game, move all the pebbles which aren't reachable by others
def aux_move_pebbles(tree, network, conf, param):
    if param["verbose"]:
        print("\n", param["facts"] ,"(",param["depth"],")", conf, "\n")

        
    # Merge the pebbles on the same node
    list_conf = merge_all(conf, tree, param)
    if list_conf is None:
        return None
        
    # Don't move the pebbles reachable by others
    tmpConf  = conf.copy()
    for p1 in conf:
        if not conf[p1][1]:
            # and don't move inactive pebbles
            tmpConf[p1] = None
        else:
            for p2 in conf:
                if conf[p2][1]:
                    if conf[p1][0] in network[conf[p2][0]]["reachable"]:
                        if param["verbose"]:
                            print(p1, "reachable by", p2)
                        tmpConf[p1] = None
                        break

    # Move the the "front-active" pebbles
    S = []
    indices = []
    next_conf = conf.copy()
    for c in tmpConf:
        if tmpConf[c] is not None :
            if network[tmpConf[c][0]]["type"] != "root":
                #for each pebble which is not already on the root
                if param["verbose"]:
                    print("MOVE ",c, "from ", tmpConf[c][0], " to ", (network[tmpConf[c][0]]["parents"]))
                if network[tmpConf[c][0]]["type"] == "reticulation":
                    #if it's on a reticulation, remember we can go right or left
                    S.append((network[tmpConf[c][0]]["parents"]))
                    indices.append(c)
                else:
                    #if it's on a tree-node, move on
                    next_conf[c] = network[tmpConf[c][0]]["parents"][0], True

    #produce all the possible configurations we can go next with a cartesian product on all the choices on reticulation nodes
    prod_S = list(itertools.product(*S))
    tc = []
    for p in prod_S:
        #for each set of choice,
        j = 0
        nc = next_conf.copy()
        for i in indices:
            # associate each choice to the right pebble
            nc[i] = (p[j],True)
            j+=1
        if nc != conf and nc is not None:
            #if it's a valid configuration, add it to the list to return
            tc.append((nc, param))

    return tc

# given a configuration checks if the root of the pattern is associated to the root of the graph
def aux_root(tree, network, conf):
    if conf[("root", tree["root"]["children"])][0]=="root":
        return True
    return False


# Do the moving from a configuration 
def move_pebbles(tree, network, conf, verbose):
    param = {"verbose":verbose,\
             "facts":0,\
             "depth":0,\
             "map":{},\
             "max_depth":0}
    Done = set([])    # the set of configurations already computed
    ToBeDone =[(conf, param)]    # the set of configurations (and the associated parameters) waiting to be computed
    HashToBeDone = set([str(conf)])    # hashkeys of the configurations to be done
    while len(ToBeDone) >0 :
        # At each step, take a configuration from the set to be done,
        tmpconf, p = ToBeDone.pop()
        utils.update_params(param, p)
        # put it in the set done, remove it from the keys of tobedone and compute it
        Done.add(str(tmpconf))
        HashToBeDone.remove(str(tmpconf))
        toAdd = aux_move_pebbles(tree, network, tmpconf, p)
        # the result of aux_move_pebbles can be either None if th computed configuration wasn't valid or a list of configurations to compute
        if toAdd is not None:
            for (xc,xp) in toAdd:
                # for each configuration returned,
                if aux_root(tree, network, xc):
                    # check if we attained a winning configuration
                    param["map"] = {}
                    for c in xc:
                        param["map"][c[0]] = xc[c][0]
                    return True, xp
                # else check if it was already computed or in the set of conf to be done
                if not str(xc) in Done \
                   and not str(xc) in HashToBeDone :
                    # if it isn't, add it to the set to be done
                    xp = deepcopy(xp)
                    ToBeDone.append((xc,xp))
                    HashToBeDone.add(str(xc))
    param["pb"] = "Didn't reach a winning configuration"
    return False, param

# construct a network from the list of edges in the given file
def get_graph(filename):
    f_in = open(filename, "r")
    lines = f_in.readlines();
    graph = {}
    arcs = set()
    for line in lines:
        #try:
        a,b = line.strip().split(" ")
        a,b = clean_name(a), clean_name(b)
        arcs.add((a,b))
        graph = add_to_graph(a,b,graph, attribute="reachable")
        #except:
        #    print(line)
    f_in.close();
    #print(graph)
    return graph, arcs

if __name__ == "__main__":
    if len(sys.argv) == 3:
        contained, param = tc(sys.argv[1], sys.argv[2])
        utils.print_res(sys.argv[1], contained, param)
    elif len(sys.argv) == 4 and sys.argv[3] in ["--verbose", "-v"]:
        print(tc(sys.argv[1], sys.argv[2], verbose=True))
    else :
        utils.print_usage("block_total_bu.py")
