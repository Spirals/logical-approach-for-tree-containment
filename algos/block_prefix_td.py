# TD-PC-CBM
#
# tree : binary, for every node outdegree = 0 (leaf) or outdegree = 2 but outdegre != 1
# network : type of nodes : 
#   root          : indegree = 0, outdegree > 0
#   leaves        : indegree > 0, outdegree = 0
#   reticulations : indegree > 0, outdegree = 1
#   tree nodes    : indegree = 1, outdegree > 1

import sys
import construct_graph
from itertools import product
import time
import copy
import utils
from init_tc import BM_PC_TD as init_game

# Return True if the given tree is contained in the given network, False otherwise
def tc(f_tree, f_network, verbose=False):
    start_init = time.time()
    tree = get_graph(f_tree)
    network = get_graph(f_network)
    start = time.time()
    done, res = init_game(tree, network)
    if not done:
        return False, {"max_depth":0, "facts":0, "pb":res, "time":time.time()-start, "loading":start-start_init}
    tree, network, possible_confs, leaves = res
    contained, param = move_pebbles(tree, network, possible_confs, leaves, verbose)
    end = time.time()
    param["time"] = end-start
    param["loading"] = start_init-start
    return contained, param

# Do the moving from a configuration 
def move_pebbles(tree, network, conf, leaves, verbose):
    param = {"verbose":verbose,\
             "facts":0,\
             "map":{},\
             "depth":0,\
             "max_depth":0}
    Done = set([])    # the set of configurations already computed
    HashToBeDone = set([str((conf, param["map"]))])    # the set of configurations (and the associated parameters) waiting to be computed
    ToBeDone =[(conf, param)]    # hashkeys of the configurations to be done
    while len(ToBeDone) >0 :
        # At each step, take a configuration from the set to be done,
        tmpconf, p = ToBeDone.pop()
        param["facts"] += 1
        p["depth"] += 1
        p["facts"] = param["facts"]
        if p["depth"] > param["max_depth"]:
            param["max_depth"] = p["depth"]
        p["max_depth"] = param["max_depth"]
        # put it in the set done, remove it from the keys of tobedone and compute it
        Done.add(str((tmpconf, p["map"])))
        HashToBeDone.remove(str((tmpconf,p["map"])))
        toAdd = aux_move_pebbles(tree, network, tmpconf, leaves, p)
        # the result of aux_move_pebbles can be either None if th computed configuration wasn't valid or a list of configurations to compute
        if toAdd is not None:
            for (xc,xp) in toAdd:
                # for each configuration returned,
                if aux_leaves(tree, network, xc, leaves):
                    # check if we attained a winning configuration
                    return True, xp
                # else check if it was already computed or in the set of conf to be done
                if not (str((xc,xp["map"])) in Done) \
                   and not (str((xc,xp["map"])) in HashToBeDone) :
                    # if it isn't, add it to the set to be done
                    xp = copy.deepcopy(xp)
                    ToBeDone.append((xc,xp))
                    HashToBeDone.add(str((xc, xp["map"])))
    param["pb"] = "Didn't reach a winning configuration"
    return False, param



# given a configuration of the pebble game, move all the pebbles which aren't reachable by others
def aux_move_pebbles(tree, network, conf, leaves, param):
    if param["verbose"]:
        print("\n", param["facts"] ,"(",param["depth"],")", "\t", param["map"], "\t", conf, "\n")

    # Exit if two pebbles are on the same node
    list_conf = list(conf.keys()).copy()
    for i, p1 in enumerate(list_conf):
        for p2 in list_conf[i+1:]:
            if conf[p1]!= None and conf[p1]==conf[p2]:
                # the pebbles are on the same node
                if conf[p1] not in param["map"]\
                   or param["map"][conf[p1]] != p1[0]\
                   or param["map"][conf[p1]] != p2[0]:
                    # if they didn't just split, return None
                    if param["verbose"]:
                        print("\tFalse: 2 pebbles ({} and {}) on the same node {}".format(p1, p2, conf[p1]))
                    return None
      
    # Don't move the pebbles reachable by others
    tmpConf  = conf.copy()
    for p1 in conf:
        if conf[p1] is not None:
            for p2 in conf:
                if conf[p2] is not None:
                    if conf[p1] in network[conf[p2]]["reachable"]:
                        if param["verbose"]:
                            print(p1, "reachable by", p2)
                        tmpConf[p1] = None

    # Move the the "front-active" pebbles
    S = []
    indices = []
    next_conf = conf.copy()
    for c in tmpConf:
        if tmpConf[c] is not None :
            if network[tmpConf[c]]["type"] != "leaf":
                #for each pebble which is not already on a leaf
                if network[tmpConf[c]]["type"] == "tree_node":
                    #if it's on a tree_node, remember we can go right, left 
                    # or split if the node is'nt already mapped
                    # and if the pebble doesn't lead to a leaf
                    children = network[tmpConf[c]]["children"]
                    if conf[c] not in param["map"] and tree[c[1]]["type"] != "leaf":
                        if param["verbose"]:
                            print("MOVE ", c, "from ", tmpConf[c],\
                                  " to ", network[tmpConf[c]]["children"],\
                                  " or split")
                        S.append((children[0], children[1], None))
                    else:
                        if param["verbose"]:
                            print("MOVE ", c, "from ", tmpConf[c],\
                                  " to ", network[tmpConf[c]]["children"])
                        S.append(children)
                    indices.append(c)
                else:
                    if param["verbose"]:
                        print("MOVE ", c, "from ", tmpConf[c],\
                              " to ", network[tmpConf[c]]["children"][0])
                    #if it's on a reticulation, move on
                    next_conf[c] = network[tmpConf[c]]["children"][0]

    #produce all the possible configurations we can go next with a cartesian product on all the choices on tree nodes
    prod_S = list(product(*S))
    tc = []
    for p in prod_S:
        #for each set of choice,
        j = 0
        nc = next_conf.copy()
        np = copy.deepcopy(param)
        for i in indices:
            # associate each choice to the right pebble
            nc[i] = p[j]
            if p[j] == None:
                # split the pebble
                np["map"][conf[i]] = i[1]
                if param["verbose"]:
                    print("MAP ", i[1], " with ", conf[i])
                nc[(i[1], tree[i[1]]["children"][0])] = conf[i]             
                nc[(i[1], tree[i[1]]["children"][1])] = conf[i]
            j+=1
        if nc != conf and nc is not None:
            #if it's a valid configuration, add it to the list to return
            tc.append((nc,np))
    return tc
            
# given a configuration checks if the leaves of the pattern are associated to the leaves of the graph
def aux_leaves(tree, network, conf, leaves):
    for l in leaves:
        if conf[(tree[l]["parents"][0], l)] is None:
            return False
        elif conf[(tree[l]["parents"][0], l)]!=l:
            return False
    return True

# construct a network from the list of edges in the given file
def get_graph(filename):
    f_in = open(filename, "r")
    lines = f_in.readlines();
    graph = {}
    for line in lines:
        try:
            a,b = line.strip().split(" ")
            a,b = construct_graph.clean_name(a), construct_graph.clean_name(b)
            graph = add_to_graph(a,b,graph)
        except:
            print(line)
    f_in.close();
    return graph

# Add en edge (a,b) to the graph structure
def add_to_graph(a,b,graph):
    if a not in graph:
        graph[a] = {"parents":[], "children":[], "reachable":set(), "type":""}
    if b not in graph:
        graph[b] = {"parents":[], "children":[], "reachable":set(), "type":""}

    graph[a]["children"].append(b)
    construct_graph.update_children(graph, a)
    if len(graph[a]["parents"])==0:
        graph[a]["type"]="root"
    elif len(graph[a]["children"])<2:
        graph[a]["type"]="reticulation"
    else:
        graph[a]["type"]="tree_node"
        
    graph[b]["parents"].append(a)
    construct_graph.update_parents(graph, b)
    graph[b]["type"]="reticulation"
    if len(graph[b]["children"])>1:
        graph[b]["type"]="tree_node"
    elif len(graph[b]["children"])==0:
        graph[b]["type"]="leaf"
    return graph
        

if __name__ == "__main__":
    if len(sys.argv) == 3:
        contained, param = tc(sys.argv[1], sys.argv[2])
        utils.print_res(sys.argv[1], contained, param)
    elif len(sys.argv) == 4 and sys.argv[3] in ["--verbose", "-v"]:
        print(tc(sys.argv[1], sys.argv[2], verbose=True))
    else :
        utils.print_usage("block_prefix_td.py")
