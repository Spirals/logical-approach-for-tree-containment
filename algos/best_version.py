# bottom-up algorithms for the tree containment problem.
# Moves as much pebble as possible with each iterations.
#
# BU-AC-CBM : Bottom-up, not conflicting block movement, active configuration (active pebbles with from where it comes)
#

import sys
#from algos.construct_graph import get_graph
from construct_graph import get_graph
import time
import copy
#import algos.utils as utils
import utils
from init_tc import BM_AC_BU as init_game
from move import move_block


# Return True if the given tree is contained in the given network, False otherwise
def tc(f_tree, f_network, verbose=False):
    start_load = time.time()
    tree = get_graph(f_tree)
    #print("tree")
    network = get_graph(f_network, attribute="reachable")
    #print("network")
    start = time.time()
    done, res = init_game(tree, network)
    #print("init")
    after_init = time.time()

    if not done:
        return False, {"max_depth":0, "facts":0, "pb":res, "time":time.time()-start,"depth":0, "loading":start-start_load, "init":after_init-start}

    tree, network, conf = res
    if verbose:
        utils.print_graphs(tree, network)
        print("conf",conf)

    contained, param = move_pebbles(tree, network, conf, verbose)
    end = time.time()

    param["time"] = end-after_init
    param["init"] = after_init-start
    param["loading"] = start-start_load
    param["total"] = end-start
    return contained, param

                        
# Do the moving from a configuration 
def move_pebbles(tree, network, conf, verbose):
    param = {"verbose":verbose,\
             "facts":0,\
             "map":{},\
             "depth":0,\
             "max_depth":0}
    Done = set([])    # the set of configurations already computed
    HashToBeDone = set([str(conf)])    # the set of configurations waiting to be computed
    ToBeDone =[(conf, param)]    # hashkeys of the configurations to be done
    while len(ToBeDone) >0 :
        if param["verbose"]:
            #utils.print_state(Done, ToBeDone, HashToBeDone)
            utils.print_state(set([]), ToBeDone, set([]))
        # At each step, take a configuration from the set to be done,
        tmpconf, p = ToBeDone.pop()
        utils.update_params(param, p)
        # put it in the set done, remove it from the keys of tobedone and compute it
        Done.add(str(tmpconf))
        HashToBeDone.remove(str(tmpconf))
        #toAdd = aux_move_pebbles(tree, network, tmpconf, p)
        toAdd = move_block(tree, network, tmpconf, p)
        # the result of aux_move_pebbles can be either None if th computed configuration wasn't valid or a list of configurations to compute
        if toAdd is not None:
            for (xc,xp) in toAdd:
                # for each configuration returned,
                if utils.aux_root(tree, network, xc):
                    # check if we attained a winning configuration
                    return True, xp
                # else check if it was already computed or in the set of conf to be done
                if not (str(xc) in Done) \
                   and not (str(xc) in HashToBeDone) :
                    # if it isn't, add it to the set to be done
                    xp = copy.deepcopy(xp)
                    ToBeDone.append((xc,xp))
                    HashToBeDone.add(str(xc))
    param["pb"] = "Didn't reach a winning configuration"
    return False, param

if __name__ == "__main__":
    if len(sys.argv) == 3:
        contained, param = tc(sys.argv[1], sys.argv[2])
        utils.print_res(sys.argv[1], contained, param)
    elif len(sys.argv) == 4 and sys.argv[3] in ["--verbose", "-v"]:
        print(tc(sys.argv[1], sys.argv[2], verbose=True))
    else :
        utils.print_usage("block_active_bu.py")
