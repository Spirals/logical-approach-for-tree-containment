import re

# construct a network from the list of edges in the given file
# attribute : "reachable", "level"
def get_graph(filename, attribute=None, arcs=False):
    f_in = open(filename, "r")
    lines = f_in.readlines();
    graph = {}
    for line in lines:
#        try:
        a,b = line.strip().split(" ")
        a,b = clean_name(a), clean_name(b)
        graph = add_to_graph(a,b,graph, attribute)
        #except:
        #    print("error in constructing the graph:",line)
    f_in.close();
    return graph

def clean_name(node):
    node = re.sub('\W+','', node.lower())
    if node[0].isdigit():
        node = "v"+node
    return node

# Add en edge (a,b) to the graph structure
def add_to_graph(a,b,graph, attribute):
    if a not in graph:
        graph[a] = {"parents":[], "children":[], "type":""}
        if attribute == "reachable":
            graph[a]["reachable"] = set()
        elif attribute == "level":
            graph[a]["level"] = 0
    if b not in graph:
        graph[b] = {"parents":[], "children":[], "type":""}
        if attribute == "reachable":
            graph[b]["reachable"] = set()
        elif attribute == "level":
            graph[b]["level"] = 0

    graph[a]["children"].append(b)
    if attribute == "reachable":
        update_children(graph, a)
    elif attribute == "level":
        find_level(graph, a)

    if len(graph[a]["parents"])==1:
        graph[a]["type"]="tree_node"
    if len(graph[a]["parents"])==0:
        graph[a]["type"]="root"
    if len(graph[a]["parents"])==2:
        graph[a]["type"]="reticulation"
        
    graph[b]["parents"].append(a)
    if attribute == "reachable":
        update_parents(graph, b)
    elif attribute == "level":
        update_level_parents(graph, b)

    if len(graph[b]["parents"])==2:
        graph[b]["type"]="reticulation"
    if len(graph[b]["parents"])==1:
        graph[b]["type"]="tree_node"
    if len(graph[b]["children"])==0:
        graph[b]["type"]="leaf"
    return graph
        
# add the node to the list of reachable nodes of all its children recursively   
def update_children(graph, a):
    for c in graph[a]["children"]:
        graph[c]["reachable"].add(a)
        graph[c]["reachable"].update(graph[a]["reachable"])
        update_children(graph, c)

# add reachable nodes to the current node and all its parents recursively recursively   
def update_parents(graph, a):
    graph[a]["reachable"].update(graph[a]["parents"])
    for p in graph[a]["parents"]:
        update_parents(graph, p)

# compute the level of the node
def find_level(graph, a):
    graph[a]["level"] = 1+max(graph[x]["level"] for x in graph[a]["children"])

# update the level of the parents of the node
def update_level_parents(graph, a):
    for p in graph[a]["parents"]:
        graph[p]["level"] = max(graph[p]["level"], graph[a]["level"]+1)
        update_level_parents(graph, p)
