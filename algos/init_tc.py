import itertools

import time
from copy import deepcopy
# The inialisation of the algorithm for the different versions 

# One Move, Active or Prefix Configuration, Bottom-Up algorithm
def OM_AC_BU(tree, network):
    conf = {}
    # add artificial root to the network and find the leaves
    leaves = set()
    nodes_net = list(network.keys()).copy()
    for n in nodes_net:
        if (network[n]["type"] == "root"):
            network["root"] = {"parents":[], "children":n, "level":network[n]["level"]+1, "type":"root"}
            network[n]["parents"] = ["root"]
            network[n]["type"] = "tree_node"
        elif (network[n]["type"] == "leaf"):
            leaves.add(n)
    
    nodes_tree = list(tree.keys()).copy()
    # initializes the game : put pebbles on the leaves and add artificial root
    for n in nodes_tree:
        tn = tree[n]
        if tn["type"]=="leaf":
            if n not in leaves:
                print("Tree leaf '%s' is not in the network."%(n))
                print("Leaves in the network :\n", leaves)
                return False, "Tree leaf '%s' is not in the network."%(n)
            conf[(tn["parents"][0], n)] = n
        elif tn["type"]=="root":
            tree["root"] = {"parents":[], "children":n, "type":"root"}
            tn["parents"] = ["root"]
            tn["type"] = "tree_node"
            conf[("root", n)]=None
    return True, (tree, network, conf)

# not conflicting Blocks of pebbles Movement, Active or Prefix Configuration, Bottom-Up algorithm
def BM_AC_BU(tree, network):
    conf = {}
    # add artificial root to the network and find the leaves
    leaves = set()
    nodes_net = list(network.keys()).copy()
    for n in nodes_net:
        if (network[n]["type"] == "root"):
            network["root"] = {"parents":[], "children":n, "reachable":set(), "type":"root"}
            network[n]["parents"] = ["root"]
            network[n]["type"] = "tree_node"
        elif (network[n]["type"] == "leaf"):
            leaves.add(n)
    
    nodes_tree = list(tree.keys()).copy()
    # initializes the game : put pebbles on the leaves and add artificial root
    for n in nodes_tree:
        tn = tree[n]
        if tn["type"]=="leaf":
            if n not in leaves:
                print("Tree leaf '%s' is not in the network."%(n))
                print("Leaves in the network :\n", leaves)
                return False, "Tree leaf '%s' is not in the network."%(n)
            conf[(tn["parents"][0], n)] = n
        elif tn["type"]=="root":
            tree["root"] = {"parents":[], "children":n, "reachable":set(), "type":"root"}
            tn["parents"] = ["root"]
            tn["type"] = "tree_node"
            conf[("root", n)]=None
    return True, (tree, network, conf)

# not conflicting Blocks of pebbles Movement, Active or Prefix Configuration, Top-Down algorithm
def BM_PC_TD(tree, network):
    conf = {}
    # add artificial root to the network and find the leaves
    leaves = set()
    nodes_net = list(network.keys()).copy()
    for n in nodes_net:
        if (network[n]["type"] == "root"):
            network["root"] = {"parents":[], "children":[n], "reachable":network[n]["reachable"], "type":"root"}
            network[n]["parents"] = ["root"]
            network[n]["type"] = "tree_node"
        elif (network[n]["type"] == "leaf"):
            leaves.add(n)
    
    nodes_tree = list(tree.keys()).copy()
    # initializes the game : add an artificial root and put a pebble on it 
    tree_leaves = []
    for n in nodes_tree:
        tn = tree[n]
        if tn["type"]=="leaf":
            if n not in leaves:
                print("Tree leaf '%s' is not in the network."%(n))
                print("Leaves in the network :\n", leaves)
                return False, "Tree leaf '%s' is not in the network."%(n)
            conf[(tn["parents"][0], n)] = None
            tree_leaves.append(n)
        elif tn["type"]=="root":
            tree["root"] = {"parents":[], "children":[n], "reachable":set(), "type":"root"}
            tn["parents"] = ["root"]
            tn["type"] = "tree_node"
            conf[("root", n)]="root"
    return True, (tree, network, conf, tree_leaves)

# not conflicting Blocks of pebbles Movement, Total Configuration, Bottom-Up algorithm
def BM_TC_BU(tree, network, pebbles):
    possible_confs = []
    conf = {}
    # add artificial root to the network and find the leaves
    leaves = set()
    other_nodes = set()
    nodes_net = list(network.keys()).copy()
    for n in nodes_net:
        if (network[n]["type"] == "root"):
            other_nodes.add(n)
            network["root"] = {"parents":[], "children":n, "reachable":set(), "type":"root"}
            network[n]["parents"] = ["root"]
            network[n]["type"] = "tree_node"
        elif (network[n]["type"] == "leaf"):
            leaves.add(n)
        else :
            other_nodes.add(n)
    
    nodes_tree = list(tree.keys()).copy()
    # initializes the game : put pebbles on the leaves and add artificial root
    for n in nodes_tree:
        tn = tree[n]
        if tn["type"]=="leaf":
            if n not in leaves:
                print("Tree leaf '%s' is not in the network."%(n))
                print("Leaves in the network :\n", leaves)
                return False, "Tree leaf '%s' is not in the network."%(n)
            conf[(tn["parents"][0], n)] = n, True
            pebbles.remove((tn["parents"][0], n))
        elif tn["type"]=="root":
            tree["root"] = {"parents":[], "children":n, "reachable":set(), "type":"root"}
            tn["parents"] = ["root"]
            tn["type"] = "tree_node"
            conf[("root", n)]=network["root"]["children"], True
    
    #compute all possible total configurations by putting the pebbles on all possible free nodes
    for permut_pebbles in itertools.permutations(pebbles):
        for comb_nodes in itertools.combinations(other_nodes, len(pebbles)):
            c = deepcopy(conf)
            for pebble, node in zip(permut_pebbles, comb_nodes):
                c[pebble] = node, True
            possible_confs.append(c)
    return True, (tree, network, possible_confs)
