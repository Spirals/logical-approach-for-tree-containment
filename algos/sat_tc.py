import time
from construct_graph import get_graph
from init_tc import BM_AC_BU as init_game
import glob
import os, sys, subprocess, re
TESTSET = sys.argv[1]


# Use the pairs of networks-trees in the test set (actually, the last tree associated with the network)
# to generate, in a file with the same name as the network file, suffixed by ".cnf.txt",
# a file (at the DIMACS CNF file format) storing a SAT formula 
# (hopefully) equivalent to the TreeContainment instance.


def build_sat_variables(tree, network, verbose):
    tree_arcs = []
    network_arcs = []
    artificial_root_already_there = False
    for v in tree:
        #add an artificial root to the tree, above the initial root:
        if tree[v]["type"]=="root":
            if v == "artificial_root":
                artificial_root_already_there = True
            else:
                tree_arcs.append(["artificial_root",v])
                root = v
        #if verbose: print(v+": "+str(tree[v]["children"]))
        for c in tree[v]["children"]:
            tree_arcs.append([v,c])
    if not(artificial_root_already_there):
        tree["artificial_root"] = {}
        tree["artificial_root"]["children"] = [root]
        tree[root]["parents"] = ["artificial_root"]
        tree[root]["type"] = "tree_node"
        tree["artificial_root"]["type"] = "root"
    
    artificial_root_already_there = False
    for v in network:
        #add an artificial root to the network, above the initial root:
        if network[v]["type"]=="root":
            if v == "artificial_root":
                artificial_root_already_there = True
            else:
                network_arcs.append(["artificial_root",v])
            root = v
        #if verbose: print(v+": "+str(network[v]["children"]))
        for c in network[v]["children"]:
            network_arcs.append([v,c])
    if not(artificial_root_already_there):
        network["artificial_root"] = {}
        network["artificial_root"]["parents"] = []
        network["artificial_root"]["children"] = [root]
        network[root]["parents"] = ["artificial_root"]
        network[root]["type"] = "tree_node"
        network["artificial_root"]["type"] = "root"
    
    # Build a list of all variables (one variable for each pair [arc of tree, arc of network])
    # and a dictionary to store the number of each such variable
    variable_list = []
    variable_dic = {}
    result = {}
    for an in network_arcs:
        # Match the network arc an with no arc of the tree -> create one variable
        variable_list.append([-1,an])
        # Give a number to this variable (first variable has number 1 as DIMACS CNF file format does not allow variable number 0
        variable_dic[str([-1,an])]=str(len(variable_list))
        # Match the network arc an with any arc at of the tree -> create one variable for each tree arc
        for at in tree_arcs:
            variable_list.append([at,an])
            variable_dic[str([at,an])]=str(len(variable_list))
    
    if verbose: print(str(len(variable_list))+" variables built.")
    result["network_arcs"] = network_arcs
    result["tree_arcs"] = tree_arcs
    result["variable_list"] = variable_list
    result["variable_dic"] = variable_dic
    result["tree"] = tree
    result["network"] = network
    return result


def build_uniqueness_constraints(tree, network, network_arcs, tree_arcs, variable_list, variable_dic, verbose):
    uniqueness_constraints = []
    for an in network_arcs:
        tree_arc_nb = 0
        # for every pair of tree arcs {at1, at2} (including when at2 is the "false" arc), ensure that at most one may label the network arc an
        for at1 in tree_arcs:
            i = tree_arc_nb+1
            variable_at1_an = variable_dic[str([at1,an])]
            uniqueness_constraints.append("-"+variable_at1_an+" -"+variable_dic[str([-1,an])]+" 0")
            while i<len(tree_arcs):
                at2 = tree_arcs[i]
                uniqueness_constraints.append("-"+variable_at1_an+" -"+variable_dic[str([at2,an])]+" 0")
                i += 1
            tree_arc_nb += 1
    if verbose: print(str(len(uniqueness_constraints))+" uniqueness constraints built.")
    return uniqueness_constraints


def build_vertex_constraints(tree, network, network_arcs, tree_arcs, variable_list, variable_dic, verbose):
    vertex_constraints = []
    for an in network_arcs:
        for at in tree_arcs:
            # for every possible match between an arc an of the network 
            # and an arc at of the tree, 
            # add constraints related with this matching, 
            # especially some constraints resulting from facts 
            # deduced from the fact that an is matched with at.
            variable_at_an = variable_dic[str([at,an])]
            tail = an[0]
            head = an[1]
            tailat = at[0]
            headat = at[1]
            
            # constraint about the arc leaving the (artificial) root
            if an[0] == "artificial_root":
                if at[0] == "artificial_root":
                    # if both arcs start from the artificial root
                    # then the arcs match
                    vertex_constraints.append(variable_dic[str([at,an])]+" 0")

            if len(network[head]["parents"])>1:
               # if the head of an is a hybrid vertex
               # then its head must have exactly one child c
               c = network[head]["children"][0]
               
               # the arc (c, child of c) must be labeled by at
               # match(at,an) => match(at,(head,c))
               # <=> [ not(match(at,an)) || match(at,(head,c)) ] 
               vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c]])]+" 0")
               
            else:
               # the head of an is not a hybrid vertex
               if tree[headat]["type"] == "leaf":
                  # if the head of at is a leaf:
                  if network[head]["type"] == "leaf":
                     # if the head of an is a leaf:
                     if head == headat:
                        # if both leaves are the same
                        # then the arcs match
                        vertex_constraints.append(variable_at_an+" 0")
                  else:
                     # the head of an is neither a hybrid nor a leaf
                     # so it is a tree vertex with two children c1 & c2
                     c1 = network[head]["children"][0]
                     c2 = network[head]["children"][1]
                     
                     # at has to match either (head,c1) or (head,c2)
                     # match(at,an) => match(at,(head,c1)) || match(at,(head,c2))
                     # <=> [ not(match(at,an)) || match(at,(head,c1)) || match(at,(head,c2)) ] 
                     vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c1]])]+" "+variable_dic[str([at,[head,c2]])]+" 0")

               else:
                  # the head of at is not a leaf,
                  # so it has two children tc1 and tc2
                  tc1 = tree[headat]["children"][0]
                  tc2 = tree[headat]["children"][1]
                  
                  # if the head of an has no child
                  # then an cannot be matched with at
                  # but this is already taken into account
                  # by the uniqueness constraints
                  
                  if len(network[head]["children"]) > 1:
                     # if the head of an has two childen
                     # then we call them c1 and c2
                     c1 = network[head]["children"][0]
                     c2 = network[head]["children"][1]
                     
                     # match(at,an) => (match(at,(head,c1)) || match(at,(head,c2))
                     # || ( (match((headat,tc1),(head,c1)) && match((headat,tc2),(head,c2)) )
                     # || ( (match((headat,tc1),(head,c2)) && match((headat,tc2),(head,c1)) )
                     # <=> [ not(match(at,an)) || match(at,(head,c1)) || match(at,(head,c2)) 
                     #     || match((headat,tc1),(head,c1)) || match((headat,tc1),(head,c2))
                     #     ]
                     # &&  [ not(match(at,an)) || match(at,(head,c1)) || match(at,(head,c2)) 
                     #     || match((headat,tc1),(head,c1)) || match((headat,tc2),(head,c1))
                     #     ]               
                     # &&  [ not(match(at,an)) || match(at,(head,c1)) || match(at,(head,c2)) 
                     #     || match((headat,tc2),(head,c2)) || match((headat,tc1),(head,c2))
                     #     ]
                     # &&  [ not(match(at,an)) || match(at,(head,c1)) || match(at,(head,c2)) 
                     #     || match((headat,tc2),(head,c2)) || match((headat,tc2),(head,c1))
                     #     ]
               
                     vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c1]])]+" "+variable_dic[str([at,[head,c2]])]+" "+variable_dic[str([[headat,tc1],[head,c1]])]+" "+variable_dic[str([[headat,tc1],[head,c2]])]+" 0")
                     vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c1]])]+" "+variable_dic[str([at,[head,c2]])]+" "+variable_dic[str([[headat,tc1],[head,c1]])]+" "+variable_dic[str([[headat,tc2],[head,c1]])]+" 0")
                     vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c1]])]+" "+variable_dic[str([at,[head,c2]])]+" "+variable_dic[str([[headat,tc2],[head,c2]])]+" "+variable_dic[str([[headat,tc1],[head,c2]])]+" 0")
                     vertex_constraints.append("-"+variable_at_an+" "+variable_dic[str([at,[head,c1]])]+" "+variable_dic[str([at,[head,c2]])]+" "+variable_dic[str([[headat,tc2],[head,c2]])]+" "+variable_dic[str([[headat,tc2],[head,c1]])]+" 0")
               
                    
    if verbose: print(str(len(vertex_constraints))+" vertex constraints built.")
    return vertex_constraints




# adapted from main.py
def mult_tc(verbose=True):
    tc_output = open("../results/results-sat-tc-"+TESTSET+".txt", "w")
    start_all = time.time()
    results = {}
    cpt = 0

    rets=[]
    if TESTSET == "simple" or TESTSET == "simplebinary":
        rets=[1]
    elif TESTSET == "small":
        rets=[10]
    elif TESTSET == "bigPositive" :
        rets=[30]
    elif TESTSET == "medium" :
        rets=[10,30]
    elif TESTSET == "big" :
        rets=range(10,35,5)
    elif TESTSET == "coalescent":
        rets=[50]
    elif TESTSET == "l7r30":
        rets=[30]
    elif TESTSET == "100leaves":
        rets=[100]

    for ret in rets:
        network_set=[]
        if TESTSET == "simplebinary":
            network_set = ["../data/simpleBinaryNet.txt"]
        elif TESTSET == "simple":
            network_set = ["../data/simpleNet"]
        elif TESTSET == "bigPositive":
            network_set = ["../data/randnet-L7-with-tree-OK/0198.txt"]
        elif TESTSET == "small":
            network_set = ["../data/randnet_L7_18X5/L7_R"+str(ret)+"/05863"]
        elif TESTSET == "big" or TESTSET == "medium" :
            network_set = glob.glob("../data/randnet_L7_18X5/L7_R"+str(ret)+"/*")
        elif TESTSET == "coalescent":
            network_set = glob.glob("../data/Coalescent_Networks/"+str(ret)+"_sequences/Rho_32/Results/net-32-50-00002")
        elif TESTSET == "l7r30":
            network_set = glob.glob("../data/randnet_L7_18X5/L7_R30/*")
        elif TESTSET == "100leaves":
            network_set = []
            for i in range(10):
               network_set.append("../data/net-100r-100l-10networks/0"+str(i))

        print("Loading networks...")
        for f_network in network_set:
            net_name = f_network.split('-')[-1]
            network = get_graph(f_network, attribute="reachable")
            tree_set=[]
            if TESTSET == "simplebinary":
                tree_set = ["../data/simpleTree"]
            elif TESTSET == "simple":
                tree_set = ["../data/simpleTree"]
            elif TESTSET == "bigPositive":
                tree_set = ["../data/randnet-L7-with-tree-OK/0198.txt.tree7.txt"]
            elif TESTSET == "small" or TESTSET == "medium" :
                tree_set = []
                for num_tree in range(100):
                    tree_set.append("../data/tree_relabeled/tree7_"+str(ret)+"/tree_7_"+str(num_tree))
                #tree_set.append("../data/tree_relabeled/tree7_"+str(ret)+"/tree_7_0")
            elif TESTSET == "big":
                #tree_set = glob.glob("../data/tree_relabeled/tree7_"+str(ret)+"/*")
                tree_set = glob.glob("../data/tree_relabeled/tree7_"+str(ret)+"/tree_7_0")
            elif TESTSET == "coalescent":
                tree_set = ["../data/Coalescent_Networks/"+str(ret)+"_sequences/Rho_32/Results/tree-32-"+str(ret)+"-"+net_name+".tree"+str(ret)+".txt"]
            elif TESTSET == "l7r30" :
                tree_set = []
                tree_set = glob.glob("../data/tree_relabeled/tree7_30/*")
                #for num_tree in range(100):
                #    tree_set.append("../data/tree_relabeled/tree7_30/tree_7_"+str(num_tree))
            elif TESTSET == "100leaves":
                tree_set = ["just to say it's not empty"]
            print("Networks loaded...")
            print(str(tree_set))
            if len(tree_set) > 0:
              numTree = 0
              for f_tree in tree_set:
                start = time.time()
                network = get_graph(f_network, attribute="reachable")
                if TESTSET == "100leaves":
                   f_tree = f_network+".tree100.txt"

                print(f_tree, f_network)
                
                # Prepare the tree name to name the output
                res = re.search("/([^/]+)$",f_tree)
                treeName = str(numTree)
                if res:
                   treeName = res.group(1)
                   
                output = open("../sat_instances/net"+f_network.replace('\\','/').split('/')[-1]+"-tree-"+treeName+".cnf.txt", "w")
                #"+f_network+"-"+str(numTree)+"
                if verbose:
                    print("-"*20, "\n", f_tree, f_network, "\n", "-"*20)
                tree = get_graph(f_tree)
                start = time.time()
                if verbose: print("Building variables...")
                temp = build_sat_variables(tree, network, verbose)
                network_arcs = temp["network_arcs"]
                tree_arcs = temp["tree_arcs"]
                variable_list = temp["variable_list"]          
                variable_dic = temp["variable_dic"]          
                network = temp["network"]
                tree = temp["tree"]
                if verbose:
                    i = 0
                    for var in variable_list:
                        i += 1
                        output.writelines("c variable "+str(i)+" = match("+str(var[0])+", "+str(var[1])+") 0\n")
                if verbose: print("Building uniqueness constraints...")
                uniqueness_constraints = build_uniqueness_constraints(tree, network, network_arcs, tree_arcs, variable_list, variable_dic, verbose)
                if verbose: print("Building vertex constraints...")
                vertex_constraints = build_vertex_constraints(tree, network, network_arcs, tree_arcs, variable_list, variable_dic, verbose)
                
                # output according to the DIMACS CNF file format described at http://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html
                # output satisfiability can be tested for example with https://jgalenson.github.io/research.js/demos/minisat.html
                output.writelines("p cnf "+str(len(variable_list))+" "+str(len(uniqueness_constraints)+len(vertex_constraints))+"\n")
                for c in uniqueness_constraints:
                    output.writelines(c+"\n")
                for c in vertex_constraints:
                    output.writelines(c+"\n")
                output.close()
                    
                # launch sat4j
                sat4j_path = "java -jar ~/sat-tc/sat4j/dist/CUSTOM/org.sat4j.core.jar"
                filename = os.path.realpath("../sat_instances/net"+f_network.replace('\\','/').split('/')[-1]+"-tree-"+treeName+".cnf.txt")
                print("Starting sat4j on file "+filename+"...")
                print("Executing: " + sat4j_path + " " + filename + " > " + filename + ".result.txt")
                
                try:
                   result = subprocess.check_output(sat4j_path + " " + filename + " >> " + filename + ".result.txt", shell=True, universal_newlines=True,stderr=subprocess.STDOUT)
                   # analyse the results of sat4j
                    
                except subprocess.CalledProcessError as e:
                   #raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
                   pass
                   
                resultFile = open(filename+ ".result.txt", "r").readlines()
                satisfiable = ""
                for line in resultFile:
                    if line[0]=="s":
                        satisfiable = line
                
                # remove temporary files created for and by SAT4J
                try:
                   result = subprocess.check_output("rm " + filename, shell=True, universal_newlines=True,stderr=subprocess.STDOUT)
                    
                except subprocess.CalledProcessError as e:
                   pass

                try:
                   result = subprocess.check_output("rm " + filename + ".result.txt", shell=True, universal_newlines=True,stderr=subprocess.STDOUT)
                    
                except subprocess.CalledProcessError as e:
                   pass

                stop = time.time()
                tc_output.writelines(f_tree+":"+f_network+";time="+str(stop-start)+";nb_variables="+str(len(variable_list))+";nb_uniqueness_constraints="+str(len(uniqueness_constraints))+";nb_vertex_constraints="+str(len(vertex_constraints))+";satisfiable:"+satisfiable)
                print("Time to generate the constraints: "+str(stop-start))
                output.close()
                numTree += 1
                
    tc_output.close()
mult_tc()