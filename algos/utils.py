from itertools import product

def print_graphs(tree, network):
    for n in tree:
        print(n, "\tchildren: ",tree[n]["children"], "\tparents: ",tree[n]["parents"])
    print()
    for n in network:
        print(n, "\tchildren: ",network[n]["children"], "\tparents: ",network[n]["parents"])
    print("------------------------------------------")

def print_state(Done, ToBeDone, HashToBeDone):
    print('--------------------------------------------')
    print("Done:")
    for d in Done:
        print(d)
    print("ToBeDone:")
    for tbd in ToBeDone:
        print(tbd[0],"\t", tbd[1])
    print("HashToBeDone:")
    for d in HashToBeDone:
        print(d)
    print('--------------------------------------------')

def print_usage(algo_name):
        print("Usage: python %s <f_tree> <f_network>\n Solves the tree containment problem with the tree stored as a list of arcs in <f_tree> and the network stored as a list of arcs in <f_network>\nExample : python %s ../data/simpleTree ../data/simpleNet"%(algo_name, algo_name))

def print_res(tree, contained, param):
    if contained:
        print("%s ; %f s ; facts=%i ; mapping=%s ; depth=%i ; loading=%f ; init=%f"%(tree, param["time"], param["facts"], str(param["map"]), param["depth"], param["loading"], param["init"]))
    else:
        print("%s ; %f s ; facts=%i ; problem='%s' ; depth=%i ; loading=%f s"%(tree, param["time"], param["facts"], param["pb"], param["depth"], param["loading"]))

def update_params(param, p):
        param["facts"] += 1
        p["depth"] += 1
        p["facts"] = param["facts"]
        if p["depth"] > param["max_depth"]:
            param["max_depth"] = p["depth"]
        p["max_depth"] = param["max_depth"]


# given a configuration checks if the root of the pattern is associated to the root of the graph
def aux_root(tree, network, conf):
    if conf[("root", tree["root"]["children"])] is not None:
        if conf[("root", tree["root"]["children"])]=="root":
            return True
    return False
