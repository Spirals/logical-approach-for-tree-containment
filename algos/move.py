from itertools import product
import merge

#def find_front(conf, network, param):
#    # Don't move the pebbles reachable by others
#    tmpConf  = conf.copy()
#    for p1 in conf:
#        if conf[p1] is not None:
#            for p2 in conf:
#                if conf[p2] is not None:
#                    if conf[p1] in network[conf[p2]]["reachable"]:
#                        if param["verbose"]:
#                            print(p1, "reachable by", p2)
#                        tmpConf[p1] = None
#    return tmpConf


# Find the pebbles not reachable by others and move forward the pebbles when a speed move is possible 
def find_front(conf, tree, network, param, just_merged):
    reached = []
    speed_move = {}
    # Don't move the pebbles reachable by others
    tmpConf  = conf.copy()
    for p1 in conf:
        if conf[p1] is not None:
            for p2 in conf:
                if conf[p2] is not None:
                    if conf[p1] in network[conf[p2]]["reachable"]:
                        if param["verbose"]:
                            print(p1, "reachable by", p2)
                        tmpConf[p1] = None
                        
                        # check if we can move speed up
                        if p1 not in reached:
                            reached.append(p1)
                            if p1[0] == p2[0]:
                                speed_move[p1] = p2
                        elif p1 in speed_move:
                            speed_move.pop(p1)
    
    next_conf = conf.copy()
    for p1, p2 in speed_move.items():
        tmpConf[p2] = None
        if p1 in just_merged:
            place = network[conf[p1]]["parents"][0]
        else:
            place = conf[p1]
        next_conf[(tree[p1[0]]["parents"][0], p1[0])] = place
        if param["verbose"]:
            print("MERGE (speed move) :", p2, "joins", p1, "on", place )
        param["map"][place] = p1[0]
        next_conf[p1] = None
        next_conf[p2] = None
        
    return tmpConf, next_conf


def move_front(conf, tmpConf, network, param, conf_type_active = False):
    # Move the the "front-active" pebbles
    S = []
    indices = []
    next_conf = conf.copy()
    for c in tmpConf:
        if tmpConf[c] is not None :
            if network[tmpConf[c]]["type"] != "root":
                #for each pebble which is not already on the root
                if param["verbose"]:
                    print("MOVE ",c, "from ", tmpConf[c], " to ", (network[tmpConf[c]]["parents"]))
                if network[tmpConf[c]]["type"] == "reticulation":
                    #if it's on a reticulation, we can go right or left
                    children = network[tmpConf[c]]["parents"].copy()
                    if conf_type_active:
                        #see if the next node is already occupied
                        for p in conf :
                            if conf[p] in children:
                                if p[0] != c[0]:
                                    children.remove(conf[p])
                                #ajouter aller direct à ce noeud si on peut rejoindre la bonne pebble ?
                    S.append(children)
                    indices.append(c)
                else:
                    #if it's on a tree-node, see if we can move on
                    next_conf[c] = network[tmpConf[c]]["parents"][0]
    return S, indices, next_conf

def all_conf(conf, S, indices, next_conf, choices, param):
    #produces all the possible configurations we can go next
    tc = []
    for p in choices:
        #for each set of choice,
        j = 0
        nc = next_conf.copy()
        for i in indices:
            # associate each choice to the right pebble
            nc[i] = p[j]
            j+=1
        if nc != conf and nc is not None:
            #if it's a valid configuration, add it to the list to return
            tc.append((nc, param))
    return tc

def move_block(tree, network, conf, param):#, conf_type_active = False):
    if param["verbose"]:
        print("\n", param["facts"] ,"(",param["depth"],")", "\t", param["map"], "\t", conf, "\n")

    # Merge the pebbles on the same node
    just_merged = merge.merge_all2(conf, tree, param)
    if just_merged is None:
        return None
    #if not merge.merge_all(conf, tree, param):
    #    return None

    # Don't move the pebbles reachable by others
    tmpConf, next_conf = find_front(conf, tree, network, param, just_merged)

    # Move the the "front-active" pebbles
    S, indices, next_conf = move_front(next_conf, tmpConf, network, param)

    #produce all the possible configurations we can go next with a cartesian product on all the choices on reticulation nodes
    prod_S = list(product(*S))
    tc = all_conf(conf, S, indices, next_conf, prod_S, param)
    return tc
