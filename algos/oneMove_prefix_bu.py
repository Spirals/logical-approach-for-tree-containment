# BU-PC-OM

import sys
from construct_graph import get_graph
from itertools import product
import utils
import time
import copy
from init_tc import OM_AC_BU as init_game
from merge import OM_AC_BU as merge_or_find_lowest

# Return True if the given tree is contained in the given network, False otherwise
def tc(f_tree, f_network, verbose=False):
    start_load = time.time()
    tree = get_graph(f_tree)
    network = get_graph(f_network, attribute="level")
    start = time.time()
    done, res = init_game(tree, network)
    after_init = time.time()
    if not done:
        return False, {"max_depth":0, "facts":0, "pb":res, "time":time.time()-start,"depth":0, "loading":start-start_load, "init":after_init-start}
    tree, network, conf = res
    if verbose:
        utils.print_graphs(tree, network)
    contained, param = move_pebbles(tree, network, conf, verbose)
    end = time.time()
    param["time"] = end-after_init
    param["init"] = after_init-start
    param["loading"] = start-start_load
    param["total"] = end-start
    return contained, param

# given a configuration of the pebble game, move a pebble of the higher level
def aux_move_pebbles(tree, network, conf, param):
    if param["verbose"]:
        print("\n", param["facts"] ,"(",param["depth"],")", "\t", param["map"], " : ", conf, "\n")


    # Merge the pebbles on the same node it there are else find a pebble on a lowest level
    found, res = merge_or_find_lowest(conf, tree, network, param)
    if found:
        return res
    elif res is None :
        return None
    else:
        lowest_pbl = res
    
    tc = []
    # Move the lowest pebble
    if param["verbose"]:
        print("MOVE ",lowest_pbl, "from ", conf[lowest_pbl], " to ", (network[conf[lowest_pbl]]["parents"]))
    node = conf[lowest_pbl]
    if network[node]["type"] == "reticulation":
        #if it's on a reticulation, we can go right or left
        conf[lowest_pbl] = network[node]["parents"][0]
        tc.append((conf.copy(), param))
        conf[lowest_pbl] = network[node]["parents"][1]
        tc.append((conf,param))
        return tc
    else:
        #if it's on a tree-node, move on
        conf[lowest_pbl] = network[node]["parents"][0]
        return [(conf, param)]
            
# Do the moving from a configuration 
def move_pebbles(tree, network, conf, verbose):
    param = {"verbose":verbose,\
             "facts":0,\
             "map":{},\
             "depth":0,\
             "max_depth":0}
    Done = set([])    # the set of configurations already computed
    HashToBeDone = set([str((conf, param["map"]))])    # the set of configurations (and the associated parameters) waiting to be computed
    ToBeDone =[(conf, param)]    # hashkeys of the configurations to be done
    while len(ToBeDone) >0 :
        if param["verbose"]:
            utils.print_state(Done, ToBeDone, HashToBeDone)
        # At each step, take a configuration from the set to be done,
        tmpconf, p = ToBeDone.pop()
        utils.update_params(param, p)
        # put it in the set done, remove it from the keys of tobedone and compute it
        Done.add(str((tmpconf, p["map"])))
        HashToBeDone.remove(str((tmpconf,p["map"])))
        toAdd = aux_move_pebbles(tree, network, tmpconf, p)
        # the result of aux_move_pebbles can be either None if th computed configuration wasn't valid or a list of configurations to compute
        if toAdd is not None:
            for (xc,xp) in toAdd:
                # for each configuration returned,
                if utils.aux_root(tree, network, xc):
                    # check if we attained a winning configuration
                    return True, xp
                # else check if it was already computed or in the set of conf to be done
                if not (str((xc,xp["map"])) in Done) \
                   and not (str((xc,xp["map"])) in HashToBeDone) :
                    # if it isn't, add it to the set to be done
                    xp = copy.deepcopy(xp)
                    ToBeDone.append((xc,xp))
                    HashToBeDone.add(str((xc, xp["map"])))
    param["pb"] = "Didn't reach a winning configuration"
    return False, param

if __name__ == "__main__":

    if len(sys.argv) == 3:
        contained, param = tc(sys.argv[1], sys.argv[2])
        utils.print_res(sys.argv[1], contained, param)
    elif len(sys.argv) == 4 and sys.argv[3] in ["--verbose", "-v"]:
        print(tc(sys.argv[1], sys.argv[2], verbose=True))
    else :
        utils.print_usage("oneMove_prefix_bu.py")
