# Logical Approach for Tree Containment

## About

This repository contains Python implementations of two approaches to solve the Tree Containment Problem, one based on Datalog, the other one based on a reduction to SAT and the use of a SAT solver.

A description of these approaches was provided in the following communication: 

Sarah J. Berkemer, Pierre Bourhis, Philippe Gambette, Lionel Seinturier, Marion Tommasi, **A Database Approach to Solve the Tree Containment Problem in Phylogenetic Networks**, Mathematics of Evolution-Phylogenetic Trees and Networks, Workshop 1: Foundations of Phylogenetic Networks, Institute for Mathematical Science (Singapore), 2023-09-06, https://ims.nus.edu.sg/events/mathematics_evolution/.

## Contents

### Algorithms

**Implemented algorithms:**
   * bottom-up algorithm, not conflicting block of pebbles movement, prefix condiguration (active pebbles + mapped nodes). To run, call **algos/block\_prefix\_bu.py**.
   * top-down algorithm, not conflicting block of pebbles movement, prefix condiguration. To run, call **algos/block\_prefix\_td.py**.
   * bottom-up algorithm, not conflicting block of pebbles movement, total configuration (all pebbles). To run, call **algos/block\_total\_bu.py**.
   * bottom-up algorithm, one pebble at a time movement, prefix configuration. To run, call **algos/oneMove_prefix_bu.py**.
   * bottom-up algorithm, not conflicting block of pebbles movement, active configuration. to run, call **algos/block\_active\_bu.py**.
   * bottom-up algorithm, one pebble at a time movement, active configuration. To run, call **algos/oneMove_active_bu.py**.
   * reduction to 5-SAT and use of the sat4j sat solver. To run, call **algos/sat\_tc.py**.

### Files

**algos/block\_active\_bu.py**: Bottom-up algorithm of the Tree Containment problem, with not conflicting block of pebbles movement and active configuration.

**algos/block\_prefix\_bu.py**: Bottom-up algorithm of the Tree Containment problem, with not conflicting block of pebbles movement and prefix configuration.

**algos/block\_prefix\_td.py**: Top-down algorithm of the Tree Containment problem, with not conflicting block of pebbles movement and prefix configuration.

**algos/block\_total\_bu.py**: Bottom-up algorithm of the Tree Containment problem, with not conflicting block of pebbles movement and total configuration.

**algos/oneMove_prefix_bu.py**: Bottom-up algorithm of the Tree Containment problem, with one pebble at a time movement and prefix configuration.

**algos/oneMove_active_bu.py**: Bottom-up algorithm of the Tree Containment problem, with one pebble at a time movement and active configuration.

**algos/sat\_tc.py**: Generation of a 5-SAT instance from the input instance of the Tree Containment problem, and call of the sat4j solver to solve it.

**README.md** : This file.

## Credits

All files were coded by Marion Tommasi, except `sat_tc.py` which was coded by Philippe Gambette. 